﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using POMExample.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample
{
    public class BaseTest
    {
        protected IWebDriver driver;
                        
        [SetUp]
        void RunBeforeAnyTests()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }
    }
}
