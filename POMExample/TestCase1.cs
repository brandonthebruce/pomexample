﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using POMExample.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample
{

    public class TestCase1
    {
        public IWebDriver driver;
        HomePage home = null;
        SearchResultsPage srp = null;

        [SetUp]
        public void createDriver()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void SearchTextExists()
        {
            home = new HomePage(driver);
            srp = home.searchForText("test");
            int resultnumber = srp.getResultByName("DNA");
            Console.WriteLine("23 and me result #"+resultnumber);
            Assert.GreaterOrEqual(resultnumber,0);
        }

        [Test]
        public void SearchTextDoesNotExist()
        {
            home = new HomePage(driver);
            srp = home.searchForText("test");
            int resultnumber = srp.getResultByName("monkey");
            Console.WriteLine("monkey result #" + resultnumber);
            Assert.AreEqual(resultnumber, -1);
        }

        [Test]
        public void ResultCount()
        {
            home = new HomePage(driver);
            srp = home.searchForText("test");
            srp.getResultsCount();
            //Console.WriteLine("monkey result #" + resultcount);
            //Assert.AreEqual(resultcount, -1);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
        }
    }
}
