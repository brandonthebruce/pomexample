﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample.PageObjects
{
    class HomePage : BasePage
    {
        //private IWebDriver driver;

        public HomePage(IWebDriver driver): base(driver)
        {
            //base.driver = driver;
            goToPage("http://amazon.com");
            PageFactory.InitElements(driver, this);
        }
 
        [FindsBy(How = How.ClassName, Using = "nav-input")]
        private IWebElement searchIcon;
        
        [FindsBy(How = How.Id, Using = "twotabsearchtextbox")]
        private IWebElement searchTextField;
        

        public void goToAmazon()
        {
            driver.Navigate().GoToUrl("http://amazon.com");
        }

        public SearchResultsPage searchForText(String searchText)
        {
            searchTextField.SendKeys(searchText);
            searchIcon.Click();
            return new SearchResultsPage(driver);
        }
     
    }
}
