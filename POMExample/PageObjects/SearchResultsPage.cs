﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample.PageObjects
{
    class SearchResultsPage : BasePage
    {
        public SearchResultsPage(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "s-result-count")]
        private IWebElement resultText;

        [FindsBy(How = How.Id, Using = "s-results-list-atf")]
        private IWebElement resultsList;

        [FindsBy(How = How.ClassName, Using = "s-result-item")]
        private IList<IWebElement> resultsArray;


        public int getResultByName(String expectedText)
        {
            for (int i = 0;i<resultsArray.Count;i++)
            {
                IList<IWebElement> details = resultsArray[i].FindElements(By.ClassName("s-access-detail-page"));
   
                if (details.Count>0)
                {
                    String resultsstring = details[0].GetAttribute("title");
                    Console.WriteLine("results:" + resultsstring + " count:" + details.Count);
                    if(resultsstring.ToLower().Contains(expectedText.ToLower()))
                    {
                        Console.WriteLine("Found at:" + i);
                        return i;
                    }
                }
      
            }
            return -1;
        }

        public void getResultsCount()
        {
            Console.WriteLine("Results Found:"+resultText.Text);
        }

        
    }

    
}
